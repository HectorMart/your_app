require 'rails_helper'

RSpec.describe DummyModel, type: :model do
  
  describe '#hola_mundo' do
    vreg = DummyModel.new
    subject {vreg.hola_mundo}
    it { is_expected.to eq('Hola') }
  end

  describe '#favorite_gadget' do
         
    my_obj = DummyModel.new
    subject {my_obj.favorite_gadget}
    it { is_expected.to eq(45) }
  end

  describe '#crea_item' do
         
    my_item = DummyModel.new
    subject {my_item.crea_item}
    it { is_expected.to eq(5) }
  end

end

